

// Add custom texts for CTA's buttons:

var ctaLink = document.querySelectorAll('.cta--link');
for (var i = 0; i < ctaLink.length; i++) {
	ctaLink[i].innerText = "shop now";
}

// Prevent default behaviour for every <a> tag because there is no href property:

links = document.querySelectorAll('a');

for (var i = 0; i < links.length; i++) {
	links[i].addEventListener('click',function(e){
		e.preventDefault();
	});
}

// Desktop menu layout adjustments for screens below 1366px :

var header = document.querySelector('header');
var sales = document.querySelector('#sales');
var work = document.querySelector('#work');
var community = document.querySelector('#community');

// On load adjustments:

window.addEventListener('load', function(){
	if (window.innerWidth <= 1420 && window.innerWidth > 1280) { 
			sales.innerText = "Sales";
			work.innerText = "Work";
	} 
	else if (window.innerWidth <= 1280 ) {
		sales.innerText = "Sales";
		work.innerText = "";
		community.innerText = "";
	} else {
		sales.innerText = "Sales 1-855-253-6686";
		work.innerText = "For Work";
		community.innerText = "Community";
	}

});

// On resize adjustments:

window.addEventListener('resize',function(){
	if (window.innerWidth <= 1420 && window.innerWidth > 1280) { 
		sales.innerText = "Sales";
		work.innerText = "Work";
	} 
	else if (window.innerWidth <= 1280 ) {
		sales.innerText = "Sales";
		work.innerText = "";
		community.innerText = "";
	} else {
		sales.innerText = "Sales 1-855-253-6686";
		work.innerText = "For Work";
		community.innerText = "Community";
	}
});


// Stretch Navigation + Lenovo icon change on scroll

icon = document.querySelector('#lenovoIcon');
menu = document.querySelector('.menu-desktop');
logoContainer = document.querySelector('#logo-container');
secondList  = document.querySelector('.second-list');
firstRow = document.querySelector('.first-row');
secondRow = document.querySelector('.second-row');

window.addEventListener('scroll',function(e){
	if(scrollY>75){
		icon.src = "img/lenovo-small.png";
		menu.classList.add('fixed');
		menu.style.height = "50px";
		logoContainer.style.height = "50px";
		secondList.classList.add('stretched');
		firstRow.classList.add('stretched-row');
		secondRow.classList.add('stretched-row');
	} else {
		icon.src = "img/lenovo-large.png";
		menu.classList.remove('fixed');
		menu.style.height = '75px';
		logoContainer.style.height = "200px";
		secondList.classList.remove('stretched');
		firstRow.classList.remove('stretched-row');
		secondRow.classList.remove('stretched-row');
	}
});

// Show hidden pens section:

var hiddenSection = document.querySelector('.hidden-products');
var seeMore = document.querySelector('.see-more');

seeMore.addEventListener('click',function(e){
	e.preventDefault();
	hiddenSection.classList.toggle("hidden");
})


// Show hidden mobile-menu <ul> :

var breadcrumb = document.querySelector('.breadcrumb');
var hiddenMenu = document.querySelector('.hidden-menu');

breadcrumb.addEventListener('click',function(){
	console.log("firstClick");
	if(hiddenMenu.style.display === "none") {
		console.log("secondClick");
		hiddenMenu.style.display = "flex";
	} else {
		console.log("secondClick");
		hiddenMenu.style.display = "none";
	}


});


// Sticky mobile menu behaviour :

var mobileMenu = document.querySelector('.mobile-menu');
window.addEventListener('scroll',function(){
	if(scrollY>75){
		mobileMenu.classList.add('fixed');
	} else {
		mobileMenu.classList.remove('fixed');
	}

});